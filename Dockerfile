FROM node:12-alpine

WORKDIR /usr/app
# copy dist to docker file
COPY . /usr/app/
# install dependencies
RUN npm install --silent --production

EXPOSE 80

ENTRYPOINT ["npm", "start"]
