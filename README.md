# Anime Tosho Downloader
This repository provides a node cli for downloading files from anime tosho via the "direct link".

## Cli
```shell script
Commands:
  cli.js animetosho-downloader  downloads the given url and extracts the archive

Options:
  --version             Show version number                            [boolean]
  --url, -u             The url to download                  [string] [required]
  --downloadFolder, -o  Sets the download folder (without tailing slash)
                                               [string] [default: "./downloads"]
  --overwrite           Overwrites existing files     [boolean] [default: false]
  --extractPath         The path were files are extracted to
                                               [string] [default: "./downloads"]
  --help                Show help                                      [boolean]
```

## Docker
```shell script
docker run --rm -v ~/Downloads:/downloads \
registry.gitlab.com/thorb3n/animetosho-downloader \
-- --downloadFolder /downloads --extractPath /downloads -u $DOWNLOAD_URL
```
