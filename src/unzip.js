const sevenBin = require('7zip-bin');
const Seven = require('node-7z');

/**
 * Unzips the given filename with the given password.
 *
 * @param {string} filename The path to the archive to unzip.
 * @param {string} extractPath The path where files will be extracted to.
 * @param {string} password The password to use.
 * @return {Promise<void>}
 */
function unzip(filename, extractPath, password) {
    return new Promise((resolve, reject) => {
        const extractStream = Seven.extract(filename, extractPath, {
            password,
            $bin: sevenBin.path7za,
        });
        extractStream.on('end', resolve);
        extractStream.on('error', reject)
    });
}

module.exports = unzip;
