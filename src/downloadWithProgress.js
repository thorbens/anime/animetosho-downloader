const download = require('download');
const ProgressBar = require('progress');

let barSingleton;

/**
 * @returns {ProgressBar}
 */
function getBar() {
    if (!barSingleton) {
        barSingleton = new ProgressBar('downloading [:bar] :percent', {
            complete: '=',
            incomplete: ' ',
            total: 100,
            width: 20,
        });
    }

    return barSingleton;
}

/**
 * Downloads the content of the given url to the given filepath.
 *
 * @param {string} url The url to download.
 * @param {string} downloadFolder The folder to save the file in.
 * @param {string?} filename The name of the file.
 * @return {Promise<void>}
 */
function downloadWithProgress(url, downloadFolder, filename) {
    console.log(`start download of "${url}" to file "${filename}"`);
    let lastProgress = 0;

    return download(url, downloadFolder, {
        filename,
        stream: true,
    }).on('downloadProgress', progress => {
        const currentProgress = Math.floor(progress.percent * 100);
        const bar = getBar();
        bar.tick(lastProgress < currentProgress ? 1 : 0, {
            transferred: progress.transferred,
            size: progress.total,
        });
        lastProgress = currentProgress;
        if (bar.complete) {
            console.log('\n');
        }
    });
}

module.exports = downloadWithProgress;
