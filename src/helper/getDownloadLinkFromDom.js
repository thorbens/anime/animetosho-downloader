/**
 * Extracts the download link and the url from the given document.
 *
 * @param {Document} dom
 * @returns {{password?: string, downloadUrl?: string}}
 */
function getDownloadLinkFromDom(dom) {
    const contentElement = dom.getElementById("content");
    const tr = Array.from(contentElement.querySelectorAll("tr")).find(tr => {
        const th = tr.querySelector("th");

        return th && th.innerHTML === "Encrypted Download";
    });
    if (!tr) {
        return {};
    }
    const downloadUrl = tr.querySelector("td a").href;
    const password = tr.querySelector("td input").value;

    return {
        downloadUrl,
        password,
    }
}

module.exports = getDownloadLinkFromDom;
