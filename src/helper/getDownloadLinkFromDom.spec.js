const fs = require("fs");
const path = require("path");
const JSDOM = require("jsdom").JSDOM;
const getDownloadLinkFromDom = require("./getDownloadLinkFromDom");

describe('#getDownloadLinkFromDom', () => {
    it('should return the correct downloadUrl and password', () => {
        const body = fs.readFileSync(path.join(__dirname, "..", "..", "test", "resources", "detail.html"));
        const dom = new JSDOM(body);
        const document = dom.window.document;
        const {downloadUrl, password} = getDownloadLinkFromDom(document);

        expect(downloadUrl).toEqual("https://animetosho.org/filelink/5d7763a34fb7bb241e8fe2bbbe74c361c6f7474cbf9b4a71b585e2238addfe9fffece088832094d9b81d4aafab151f9feab2ee5207bbc245f2fcb7198cb3f1d19d3ecce958d1");
        expect(password).toEqual("https://animetosho.org/file/448576");
    });
});
