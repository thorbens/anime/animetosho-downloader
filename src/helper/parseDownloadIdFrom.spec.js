const parseDownloadIdFrom = require("./parseDownloadIdFrom");

describe('#parseDownloadIdFrom', function () {
    it('should return an id', function () {
        const url = "https://animetosho.org/file/moozzi2-senkou-no-night-raid-01-bd-1920x1080.310350";
        const id = parseDownloadIdFrom(url);
        expect(id).toEqual("310350");
    });
});
