const regEx = /.*\.(.*?)$/;

/**
 * Parses the id from the given url.
 * If no id could be parsed, undefined is returned.
 *
 * @param {string} url The url to parse the id from.
 * @returns {string|undefined} The parsed id.
 */
function parseDownloadIdFrom(url) {
    const match = regEx.exec(url);

    return match ? match[1] : undefined;
}

module.exports = parseDownloadIdFrom;
