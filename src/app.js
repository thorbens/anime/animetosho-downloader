const fs = require('fs');
const getDownloadLinkFromDom = require('./helper/getDownloadLinkFromDom');
const parseDownloadIdFrom = require('./helper/parseDownloadIdFrom');
const download = require('download');
const unzip = require('./unzip');
const JSDOM = require("jsdom").JSDOM;
const got = require('got');

/**
 *
 * @param {string} url The url to download.
 * @param {string} downloadFolder The folder to save the downloaded file to.
 * @param {string} extractPath The path where files will be extracted to.
 * @param {boolean} overwrite Force overwrites of existing files.
 * @returns {Promise<void>}
 */
async function perform({url, downloadFolder, overwrite, extractPath}) {
    const downloadId = parseDownloadIdFrom(url);
    if (!downloadId) {
        throw new Error(`could not parse download id from url "${url}"`);
    }
    console.log(`parsed download id "${downloadId}"`);
    const filename = `${downloadId}.7z`;
    const password = await parseAndDownloadFromDetailUrl(url, downloadFolder, filename, overwrite);
    console.log(`start unziping of "${downloadFolder}/${filename}" to "${extractPath}"`);
    await unzip(`${downloadFolder}/${filename}`, extractPath, password);
}

/**
 * Downloads the given url and saves it to the given filename.
 *
 * @param {string} url
 * @param {string} downloadFolder
 * @param {string} filename
 * @param {boolean} overwrite Force overwrites of existing files.
 * @return {Promise<string>} The password to extract the download archive.
 */
async function parseAndDownloadFromDetailUrl(url, downloadFolder, filename, overwrite) {
    console.log(`start fetching url "${url}"`);
    const response = await got(url);
    const dom = new JSDOM(response.body);
    const document = dom.window.document;
    const {downloadUrl, password} = getDownloadLinkFromDom(document);
    console.log(`download from "${downloadUrl}" with password "${password}"`);
    let outputPath = `${downloadFolder}/${filename}`;
    if (!fs.existsSync(outputPath) || overwrite) {
        await download(downloadUrl, downloadFolder, {
            filename,
        });
        //await downloadWithProgress(downloadUrl, downloadFolder, filename);
    } else {
        console.log(`file "${outputPath} already exists, skip downloading`);
    }

    return password;
}

module.exports = {
    perform,
};
