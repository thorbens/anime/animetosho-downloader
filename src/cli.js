#!/usr/bin/env node

const app = require('./app');
const argv = require('yargs')
    .command('animetosho-downloader', 'downloads the given url and extracts the archive')
    .option('url', {
        alias: 'u',
        type: 'string',
        description: 'The url to download'
    })
    .option('downloadFolder', {
        alias: 'o',
        default: './downloads',
        type: 'string',
        description: 'Sets the download folder (without tailing slash)'
    })
    .option('overwrite', {
        type: 'boolean',
        default: false,
        description: 'Overwrites existing files'
    })
    .option('extractPath', {
        type: 'string',
        default: "./downloads",
        description: 'The path were files are extracted to'
    })
    .demandOption(['url'])
    .help()
    .argv;

const downloadFolder = argv.downloadFolder;
console.log(`files will be saved to "${downloadFolder}`);
const overwrite = argv.overwrite;
const url = argv.url;
const extractPath = argv.extractPath;

app.perform({
    url,
    downloadFolder,
    overwrite,
    extractPath,
}).catch(console.error);
